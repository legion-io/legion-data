require 'spec_helper'
# Legion::Data::Connection.setup
Legion::Data::Models.load

RSpec.describe Legion::Data::Model::Environment do
  after(:all) do
    Legion::Data::Connection.shutdown
  end

  it { should respond_to? :node }
  it { should respond_to? :user_owner }
  it { should respond_to? :group_owner }
  it { should be_a Sequel::Model }
end
