require 'spec_helper'
Legion::Data::Connection.setup
Legion::Data::Models.load

RSpec.describe Legion::Data::Model::TaskLog do
  after(:all) do
    Legion::Data::Connection.shutdown
  end

  it { should respond_to? :task }
  it { should respond_to? :node }
  it { should be_a Sequel::Model }
end
