# Legion::Data Changelog
## v1.1.2
* Updating connection details to support JDBC
* Changing default max connections to 10
* Fixing issue with Legion::Cache always throwing an error

## v0.2.0
* Redesigning schema

## v0.1.1
* Updated `rubocop.yml` with updated syntax
* Changing default connection count to 100 from 10
* Updating some exception handling to meet rubocop requirements
* Updating gemspec to have more things
* Fixing logic in debug_logging
* Adding RSpec tests for debug_logging
