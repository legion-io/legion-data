lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/data/version'

Gem::Specification.new do |spec|
  spec.name = (RUBY_ENGINE == 'jruby' ? 'legion-data-java' : 'legion-data')
  spec.version       = Legion::Data::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Used by Legion to connect to the database'
  spec.description   = 'The Legion connect gem'
  spec.homepage = 'https://bitbucket.org/legion-io/legion-data'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['bug_tracker_uri'] = 'https://bitbucket.org/legion-io/legion-data/issues?status=new&status=open'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/legion-data/src/CHANGELOG.md'
  spec.metadata['documentation_uri'] = 'https://bitbucket.org/legion-io/legion-data'
  spec.metadata['homepage_uri'] = 'https://bitbucket.org/legion-io/legion-data'
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion-data'
  spec.metadata['wiki_uri'] = 'https://bitbucket.org/legion-io/legion-data/wiki/Home'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'codecov'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'

  spec.add_dependency 'legion-logging'
  spec.add_dependency 'legion-settings'

  if RUBY_ENGINE == 'jruby'
    spec.add_dependency 'jdbc-mysql'
  else
    spec.add_dependency 'mysql2'
  end
  spec.add_dependency 'sequel'
end
