# Legion::Data

Legion::Data is used by the framework to connect to a database. All database changes should be
added as a migration with proper up/downs.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'legion-data'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install legion-data

## Usage

You can create a new connection to the database with the following example
``` Legion::Data::Connection.new.database.connection ```
Keep in mind that if you need access to the database as part of a LEX, you should instead add it as a
requirement inside your definitions with the following
```
def requirements
  %w[legion-transport legion-data]
end
```
and the framework will take care of the rest.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/legion-io/legion-data.
