Sequel.migration do
  up do
    run "CREATE TABLE `relationships` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `chain_id` int(11) unsigned DEFAULT NULL,
  `trigger_id` int(11) unsigned NOT NULL,
  `action_id` int(11) unsigned NOT NULL,
  `delay` int(11) unsigned NOT NULL DEFAULT '0',
  `allow_new_chains` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `conditions` text,
  `transformation` text,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `chain_id` (`chain_id`),
  CONSTRAINT `function_chain_id` FOREIGN KEY (`chain_id`) REFERENCES `chains` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relationship_action_id` FOREIGN KEY (`action_id`) REFERENCES `functions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `relationship_trigger_id` FOREIGN KEY (`trigger_id`) REFERENCES `functions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
  end

  down do
    drop_table :relationships
  end
end
