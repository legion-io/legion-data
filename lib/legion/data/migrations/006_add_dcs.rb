Sequel.migration do
  up do
    run "CREATE TABLE `datacenters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_owner` int(11) unsigned DEFAULT NULL,
  `group_owner` int(11) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `active` (`active`),
  KEY `datacenters_user_owner` (`user_owner`),
  KEY `datacenters_group_owner` (`group_owner`),
  CONSTRAINT `datacenters_group_owner` FOREIGN KEY (`group_owner`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `datacenters_user_owner` FOREIGN KEY (`user_owner`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;"
  end

  down do
    drop_table :datacenters
  end
end
