Sequel.migration do
  change do
    alter_table(:relationships) do
      add_column :debug, Integer, null: false, default: 0, index: true
    end
  end
end
