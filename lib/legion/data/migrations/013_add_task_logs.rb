Sequel.migration do
  up do
    run "CREATE TABLE `task_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int(11) unsigned NOT NULL,
  `function_id` int(11) unsigned,
  `node_id` int(11) unsigned,
  `entry` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY (`created`),
  KEY (`updated`),
  CONSTRAINT `task_log_task_id` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `task_log_functions` FOREIGN KEY (`function_id`) REFERENCES `functions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `task_log_nodes` FOREIGN KEY (`node_id`) REFERENCES `nodes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
  end

  down do
    drop_table :task_logs
  end
end
