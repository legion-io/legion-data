Sequel.migration do
  change do
    alter_table(:tasks) do
      add_column :delay, Integer, limit: 1, null: false, default: 0, index: true
      add_column :debug, Integer, limit: 1, null: false, default: 0, index: true
    end
  end
end
