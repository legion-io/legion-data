Sequel.migration do
  up do
    run "CREATE TABLE `chains` (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned DEFAULT '1',
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_owner` int(11) unsigned DEFAULT NULL,
  `group_owner` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `active` (`active`),
  KEY `chains_user_owner` (`user_owner`),
  KEY `chains_group_owner` (`group_owner`),
  CONSTRAINT `chains_group_owner` FOREIGN KEY (`group_owner`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `chains_user_owner` FOREIGN KEY (`user_owner`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
  end

  down do
    drop_table :chains
  end
end
