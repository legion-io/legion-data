Sequel.migration do
  change do
    alter_table(:tasks) do
      add_column :payload, :text
    end
  end
end
