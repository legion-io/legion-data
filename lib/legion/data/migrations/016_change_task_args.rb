Sequel.migration do
  change do
    alter_table(:tasks) do
      rename_column :args, :function_args
    end
  end
end
