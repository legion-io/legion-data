Sequel.migration do
  change do
    alter_table(:extensions) do
      add_column :schema_version, Integer, null: false, default: 0, index: true
    end
  end
end
