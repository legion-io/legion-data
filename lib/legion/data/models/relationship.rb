# frozen_string_literal: true

require_relative 'function'

module Legion
  module Data
    module Model
      class Relationship < Sequel::Model
        many_to_one :chain
        one_to_many :task
        many_to_one :trigger, class: Legion::Data::Model::Function
        many_to_one :action, class: Legion::Data::Model::Function
      end
    end
  end
end
