# frozen_string_literal: true

module Legion
  module Data
    module Model
      class Environment < Sequel::Model
        one_to_many :node
      end
    end
  end
end
