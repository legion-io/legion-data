# frozen_string_literal: true

module Legion
  module Data
    module Model
      class Node < Sequel::Model
        many_to_one :environment
        many_to_one :datacenter
        one_to_many :task_log
      end
    end
  end
end
