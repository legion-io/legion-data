# frozen_string_literal: true

module Legion
  module Data
    module Model
      class Datacenter < Sequel::Model
        one_to_many :node
      end
    end
  end
end
