# frozen_string_literal: true

module Legion
  module Data
    module Model
      class Chain < Sequel::Model
        one_to_many :relationship
      end
    end
  end
end
